<?php

class User 
{
    private $model;

    function __construct()
    {
        require 'model/UserModel.php';
        require 'Helper.php';
        $this->model = new UserModel();
    }

    public function index()
    {
        $data = $this->model->getData();
        $rows = [];
        while ($row = mysqli_fetch_assoc($data)) {
            $rows[] = $row;
        }

        Helper::exportJson($rows, "lấy dữ liệu thành công");
    }

    private function handlerData($data){
        $rows = [];
        while ($row = mysqli_fetch_assoc($data)) {
            $rows[] = $row;
        }
        return $rows;
    }

    public function login()
    {
        
        $username = $_POST["username"];
        $password = $_POST["password"];

        $data = $this->model->login($username, $password);

        $rows = $this->handlerData($data);

        if (count($rows) > 0) { 
            Helper::exportJson($rows, "Đăng nhập thành công");
        }else {
            Helper::exportJson($rows, "Dăng nhập thất bại", false);
        }
        
    }

    public function register()
    {
        $username = $_POST["username"];
        $password = $_POST["password"];
        $phone = $_POST["phone"];
        $avatar = $this->uploadAvatar();
        if (isset($avatar)) {
            Helper::exportJson([], "Thêm thất bại", false);
            die();
        }


        $data = $this->model->register($username, $password, $phone, $avatar);

        if ($data) { 
            Helper::exportJson([], "Thêm thành công");
        }else {
            Helper::exportJson([], "Thêm thất bại", false);
        }
    }

    public function search()
    {
        $_search = $_POST["search"];

        $data = $this->model->search($_search);
        $rows = $this->handlerData($data);


        // var_dump(count($rows) );

        if (count($rows) > 0) { 
            Helper::exportJson($rows, "Thêm thành công");
        }else {
            Helper::exportJson($rows, "Thêm thất bại", false);
        }
    }

    public function uploadAvatar()
    {


        //  var_dump(new DateTime());
        $date = new DateTime();


        // var_dump($_FILES["avatar"]);
        // die();

        // file upload.php xử lý upload file

        if ($_SERVER['REQUEST_METHOD'] !== 'POST')
        {
            // Dữ liệu gửi lên server không bằng phương thức post
            echo "Phải Post dữ liệu";
            die;
        }

        // Kiểm tra có dữ liệu avatar trong $_FILES không
        // Nếu không có thì dừng
        if (!isset($_FILES["avatar"]))
        {
            echo "Dữ liệu không đúng cấu trúc";
            die;
        }

        // Kiểm tra dữ liệu có bị lỗi không
        if ($_FILES["avatar"]['error'] != 0)
        {
            echo "Dữ liệu upload bị lỗi";
            die;
        }

        // Đã có dữ liệu upload, thực hiện xử lý file upload

        //Thư mục bạn sẽ lưu file upload
        $target_dir    = "uploads/";
        //Vị trí file lưu tạm trong server (file sẽ lưu trong uploads, với tên giống tên ban đầu)



        $target_file   = $target_dir . $date->getTimestamp() . basename($_FILES["avatar"]["name"]) ;

        $allowUpload   = true;

        //Lấy phần mở rộng của file (jpg, png, ...)
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        // Cỡ lớn nhất được upload (bytes)
        $maxfilesize   = 90000000;

        ////Những loại file được phép upload
        $allowtypes    = array('jpg', 'png', 'jpeg', 'gif');


        if(isset($_POST["submit"])) {
            //Kiểm tra xem có phải là ảnh bằng hàm getimagesize
            $check = getimagesize($_FILES["avatar"]["tmp_name"]);
            if($check !== false)
            {
                echo "Đây là file ảnh - " . $check["mime"] . ".";
                $allowUpload = true;
            }
            else
            {
                echo "Không phải file ảnh.";
                $allowUpload = false;
            }
        }
        if (file_exists($target_file))
        {
            // echo "Tên file đã tồn tại trên server, không được ghi đè";
            Helper::exportJson(null, "File exists", false);
            $allowUpload = false;
        }
        // Kiểm tra kích thước file upload cho vượt quá giới hạn cho phép
        if ($_FILES["avatar"]["size"] > $maxfilesize)
        {
            // echo "Không được upload ảnh lớn hơn $maxfilesize (bytes).";
            Helper::exportJson($target_file, "Không được upload ảnh lớn hơn $maxfilesize (bytes).", false);
            $allowUpload = false;
        }

        // Kiểm tra kiểu file
        if (!in_array($imageFileType,$allowtypes ))
        {
            echo "Chỉ được upload các định dạng JPG, PNG, JPEG, GIF";
            $allowUpload = false;
        }


        if ($allowUpload)
        {
            // Xử lý di chuyển file tạm ra thư mục cần lưu trữ, dùng hàm move_uploaded_file
            if (move_uploaded_file($_FILES["avatar"]["tmp_name"], $target_file))
            {
                // Helper::exportJson($target_file, "Upload ảnh thành công!", true);
                return $target_file;
            }
            else
            {
                Helper::exportJson(null, "Error upload file", true);
                return false;
            }
        }
        else
        {
             return false;
            // echo "Không upload được file, có thể do file lớn, kiểu file không đúng ...";
            //  Helper::exportJson(null, "Không upload được file, có thể do file lớn, kiểu file không đúng ...", true);
        }

    }
    

}


$u = new User();
$c = $_GET["func"];
$u->$c();