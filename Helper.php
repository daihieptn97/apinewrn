<?php

class Helper
{
    /**
     * @param $data 
     * @param string $message
     * @param bool $status
     */
    public static function exportJson($data, string $message = "", bool $status = true)
    {
        $res = [
            "status" => $status,
            "data" => $data,
            "message" => $message
        ];

        echo json_encode((object)$res);

    }
}